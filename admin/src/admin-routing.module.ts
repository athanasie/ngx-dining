import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancedFormItemResolver, AdvancedFormModalData } from '@universis/forms';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { DiningCardListConfiguration } from './components/dining-card-list.config';
import { DiningCardSearchConfiguration } from './components/dining-card-search.config';
import { DiningListConfiguration } from './components/dining-list.config';
import { DiningSearchConfiguration } from './components/dining-search.config';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { EditComponent } from './components/edit/edit.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { ListComponent } from './components/list/list.component';
import { MessageSearchConfiguration } from './components/message-search-configuration.config';
import { MessageListConfiguration } from './components/messages-list.config';
import { SelectReportComponent } from '@universis/ngx-reports';
import { DocumentSeriesResolverService } from './document-series-resolver.service';
import { DiningConfigurationResolver } from '@universis/ngx-dining/shared';

const routes: Routes = [
    {
        'path': '',
        'pathMatch': 'full',
        'redirectTo': 'index'
    },
    {
        path: 'index',
        component: ListComponent,
        data: {
            model: 'DiningRequestActions',
            description: 'NewRequestTemplates.DiningRequestAction.Description',
            tableConfiguration: DiningListConfiguration,
            searchConfiguration: DiningSearchConfiguration
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestActions',
              action: 'modal-edit',
              serviceQueryParams: EditComponent.ServiceQueryParams,
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          }
        ]
    },
    {
      path: 'dining-cards',
      component: CardsListComponent,
      data: {
          model: 'StudentDiningCards',
          description: 'UniversisDiningModule.DiningCardsTemplates.DiningCard.Description',
          tableConfiguration: DiningCardListConfiguration,
          searchConfiguration: DiningCardSearchConfiguration 
      },
      children: [
        {
          path: 'item/:id/edit',
          component: ModalEditCardComponent,
          outlet: 'modal',
          data: <AdvancedFormModalData>{
            model: 'StudentDiningCards',
            action: 'modal-edit',
            serviceQueryParams: EditCardComponent.ServiceQueryParams,
            closeOnSubmit: true,
            continueLink: '.'
          },
          resolve: {
            model: AdvancedFormItemResolver
          }
        },
        {
          path: 'print',
          pathMatch: 'full',
          component: SelectReportComponent,
          outlet: 'modal',
          resolve: {
            item: DiningConfigurationResolver,
            documentSeriesUrl : DocumentSeriesResolverService
          },
          data: {
            model: 'DiningConfigurations'
          }
        }
      ]
  },
  {
    path: 'messages',
    component: AdvancedListComponent,
    data: {
        model: 'DiningRequestActionMessages',
        description: 'UniversisDiningModule.Messages.Title',
        tableConfiguration: MessageListConfiguration,
        searchConfiguration: MessageSearchConfiguration
    },
    children: [
      {
        path: 'item/:id/edit',
        
        component: ModalEditComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'DiningRequestActions',
          serviceQueryParams: EditComponent.ServiceQueryParams,
          action: 'modal-edit',
          closeOnSubmit: true,
          continueLink: '.'
        },
        resolve: {
          model: AdvancedFormItemResolver
        }
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule], 
  providers:[
    DiningConfigurationResolver
  ]
})
export class AdminDiningRoutingModule { }
