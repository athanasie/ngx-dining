import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { AdminDiningRoutingModule } from './admin-routing.module';
import { TablesModule } from '@universis/ngx-tables';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { EditComponent } from './components/edit/edit.component';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { SidePreviewComponent } from './components/side-preview/side-preview.component';
import { AttachmentDownloadComponent } from './components/attachment-download/attachment-download.component';
import { ListComponent } from './components/list/list.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { RouterModalModule } from '@universis/common/routing';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ReviewComponent } from './components/review/review.component';
import { CardSidePreviewComponent } from './components/card-side-preview/card-side-preview.component';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { ReportService, ReportsSharedModule } from '@universis/ngx-reports';
import { NgxSignerModule } from '@universis/ngx-signer';
import { DocumentSeriesResolverService } from './document-series-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AdminDiningRoutingModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    RouterModalModule,
    TranslateModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    TablesModule,
    ProgressbarModule, 
    ReportsSharedModule,
    NgxSignerModule
  ],
  declarations: [
    ModalEditComponent,
    EditComponent,
    ComposeMessageComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ListComponent,
    EditCardComponent,
    ModalEditCardComponent,
    ReviewComponent,
    CardSidePreviewComponent, 
    CardsListComponent
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ReportService, 
    DocumentSeriesResolverService
  ]
})
export class AdminDiningModule {  
}
