// tslint:disable: quotemark
export const MessageListConfiguration = {
  "$schema": "https://universis.gitlab.io/ngx-tables/schemas/v1/schema.json",
  "model": "DiningRequestActionMessages",
  "title": "UniversisDiningModule.Messages.Title",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(action/requestNumber eq  '${text}' or indexof(sender/alternateName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "action",
      "property": "action",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-envelope text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${action}", "edit"]
            }
          }],
        "navigationExtras": {
          "fragment": "show-messages"
        }
      }
    },
    {
      "name": "sender/alternateName",
      "property": "senderName",
      "title": "UniversisDiningModule.Messages.Sender",
      "formatter": "TemplateFormatter",
      "formatString": "${senderName}"
    },
    {
      "name": "recipient/alternateName",
      "property": "sentByStudent",
      "title": "UniversisDiningModule.Messages.SentByStudent",
      "formatter": "TemplateFormatter",
      "formatString": "${sentByStudent}",
      "hidden": true
    },
    {
      "name": "recipient/alternateName",
      "property": "recipientName",
      "title": "UniversisDiningModule.Messages.Recipient",
      "formatter": "TemplateFormatter",
      "formatString": "${recipientName}",
      "hidden": true
    },
    {
      "name": "subject",
      "property": "subject",
      "title": "UniversisDiningModule.Messages.Subject",
      "formatter": "TemplateFormatter",
      "formatString": "${subject}"
    },
    {
      "name": "action/requestNumber",
      "property": "requestNumber",
      "title": "Requests.RequestNumber"
    },
    {
      "name": "action/actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'",
              "text-primary": "'${actionStatus}'==='PotentialActionStatus'",
            }
          }
        }
      ]
    },
    {
      "name": "action/effectiveStatus/alternateName",
      "property": "effectiveStatus",
      "title": "Requests.EffectiveStatus",
      "className": "text-center",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${effectiveStatus == null ? 'UnknownStatus' : effectiveStatus}"
        },
        {
          "formatter": "TranslationFormatter",
          "formatString": "EffectiveStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${effectiveStatus}' === 'RejectedAttachmentsEffectiveStatus' || '${effectiveStatus}' === 'InvalidRequestDataEffectiveStatus'",
              "text-success": "'${effectiveStatus}' === 'AcceptedAttachmentsEffectiveStatus'"
            }
          }
        },
      ]
    },
    {
      "name": "action/agent/name",
      "property": "agent",
      "title": "Requests.Edit.claim"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "student/department/name",
      "property": "studentDepartment",
      "title": "Students.Department",
    },
    {
      "name": "dateCreated",
      "property": "dateCreated",
      "title": "UniversisDiningModule.Date",
      "formatter": "DateTimeFormatter",
      "formatString": "short"
    },
  ],
  "defaults": {
    "orderBy": "dateCreated desc",
    "expand": "sender,recipient",
  },
  "paths": [
    {
      "name": "Requests.Active",
      "show": true,
      "alternateName": "list/active",
      "filter": {
      }
    },
    {
      "name": "Requests.All",
      "show": true,
      "alternateName": "list/index",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.ExamPeriodParticipateAction",
      "show": true,
      "alternateName": "list/examParticipate",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestDocumentActions",
      "show": true,
      "alternateName": "list/documentAction",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestsRemoveAction",
      "show": false,
      "alternateName": "list/remove",
      "filter": {
      }
    },
    {
      "name": "StudentRequestActionsTypes.RequestsSuspendAction",
      "show": false,
      "alternateName": "list/suspend",
      "filter": {
      }
    },
    {
      "name": "Requests.AllDepartments",
      "show": true,
      "alternateName": "list/allDepartments",
      "filter": {
      }
    }
  ],
  "criteria": [
    {
      "name": "senderName",
      "filter": "(indexof(sender/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "recipientName",
      "filter": "(indexof(recipient/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "sentByStudent",
      "filter": "(indexof(recipient/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "actionStatus",
      "filter": "(action/actionStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "effectiveStatus",
      "filter": "(action/effectiveStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(action/requestNumber, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "agent",
      "filter": "(indexof(action/agent/name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(indexof(student/department/name,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')"
    },
    {
      "name": "maxDate",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/id eq 1)' : '(student/studentStatus/id ne 1)'}", 
      "type": "text"
    },
  ],
  "searches": []
};
