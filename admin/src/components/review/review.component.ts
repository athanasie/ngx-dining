import { Component, OnInit, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ConfigurationService, ErrorService } from '@universis/common';

@Component({
  selector: 'lib-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  @Input() model;
  public currentLang: any;
  editingReview: any = false;

  constructor(private _configurationService: ConfigurationService,
    private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _appEvent: AppEventService
    ) { }

  ngOnInit(): void {
    this.currentLang = this._configurationService.currentLocale;
    if (this.model) {
      this.model.totalFamilyIncome = ( (this.model.guardianFinancialAttributes ? (this.model.guardianFinancialAttributes.filter(function (x) { return typeof x.totalIncome === 'number' }).reduce(function (previous, current) { return previous + current.totalIncome; }, 0)): 0) + ((this.model.studentSubmitsTaxReportInGreece === true ) ? this.model.studentTotalIncome : 0) + (this.model.siblingFinancialAttributes ? (this.model.siblingFinancialAttributes.filter(function (x) { return typeof x.totalIncome === 'number' }).reduce(function (previous, current) { return previous + current.totalIncome; }, 0)): 0) + (typeof this.model.studentGuardianTotalIncome === 'number' && (this.model.guardianType.alternateName === 'mother' || this.model.guardianType.alternateName === 'father') ? this.model.studentGuardianTotalIncome: 0) + (this.model.childrenFinancialAttributes ? (this.model.childrenFinancialAttributes.filter(function (x) { return typeof x.totalIncome === 'number' }).reduce(function (previous, current) { return previous + current.totalIncome; }, 0)):0) + ((this.model.studentSpouseTotalIncome ? this.model.studentSpouseTotalIncome : 0)) );
    }
}

  onEditReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        this.editingReview = false;
        // update data
        const findItem = this.model.courseRegistrations.find((item) => {
          return item.id === result.itemReviewed;
        });
        if (findItem) {
          if (findItem.review) {
            Object.assign(findItem.review, result);
          } else {
            findItem.review = result;
          }
          // send an application event
          this._appEvent.change.next({
            model: 'CourseClassRegisterActions',
            target: findItem
          });
        }

      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });

    }
  }

  onItemReviewEvent(event) {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`).save(event.data).then((result) => {
        return this._context.model(`DiningRequestActions/${event.data.itemReviewed}/review`)
          .asQueryable().expand('createdBy', 'modifiedBy').getItem().then((finalResult) => {
            this.editingReview = false;
            this.model.review = finalResult;
          });
      }).catch((err) => {
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }


}
