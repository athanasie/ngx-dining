import { Component, Input, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService } from '@universis/common';

@Component({
  selector: 'lib-card-side-preview',
  templateUrl: './card-side-preview.component.html',
  styleUrls: ['./card-side-preview.component.css']
})
export class CardSidePreviewComponent implements OnInit {
  paramSubscription: any;

  @Input() model;
  public currentLang: any;

  constructor(private _configurationService: ConfigurationService,
    private _context: AngularDataContext) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
    if (this.model) {
//
    }
  }
}

