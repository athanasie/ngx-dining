import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild, TemplateRef, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ModalService, DIALOG_BUTTONS, LoadingService, ErrorService, UserService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AttachmentDownloadComponent } from '../attachment-download/attachment-download.component';
import { Subscription } from 'rxjs';
@Component({
  selector: 'dining-edit-action',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss', '../../../../src/lib/dining.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {

  public static readonly ServiceQueryParams = {
    $expand: 'agent,messages,childrenFinancialAttributes,siblingFinancialAttributes,guardianFinancialAttributes,review,effectiveStatus,modifiedBy,student($expand=person,department,studentStatus,inscriptionMode,studyProgram($expand=studyLevel)),attachments($expand=attachmentType)'
  };

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('attachmentDownload') attachmentDownload?: AttachmentDownloadComponent;
  @ViewChild('form') form?: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public modalRef!: BsModalRef;
  public attachmentToReject: any;  
  public messages: any[] | undefined;
  public otherApplications: any[] | undefined;
  @Input() user: any;
  newMessage: any = {
    attachments: []
  };
  public showNewMessage = false;
  public fragmentSubscription: Subscription | undefined;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _modalService: BsModalService,    
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService,
    private _userService: UserService) { }

  ngOnChanges(changes: SimpleChanges): void {
   //
  }
  
  ngOnInit(): void {
    //
  }

  ngAfterViewInit() {
    this._userService.getUser().then((user) => {
      this.user = user;
      this.paramSubscription = this._activatedRoute.queryParams.subscribe((query) => {
        if (query['download']) {
          this.attachmentDownload?.show({
            url: query['download'] 
          });
        } else {
          this.attachmentDownload?.hide();
        }
      });
      this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
        this.model = data['model'];
        if (this.form) {
          this.form.refreshForm.emit({
            submission: {
              data: this.model
            }
          });
        }
        // get messages
        if (this.model) {

          // get other active applications of the same user
          this._context.model('DiningRequestActions')
            .where('owner').equal(this.model.owner)
            .and('actionStatus/alternateName').equal('ActiveActionStatus')
            .orderByDescending('dateModified')
            .getItems().then((results) => {
              this.otherApplications = results.filter((item) => {
                return item.id !== this.model.id;
              });
            });
  
          this._context.model(`DiningRequestActions/${this.model.id}/messages`)
            .asQueryable()
            .orderBy('dateCreated desc')
            .expand('attachments')
            .getItems().then((results) => {
              this.messages = results;
              this.fragmentSubscription = this._activatedRoute.fragment.subscribe((fragment) => {
                  if (fragment) {
                    let el = document.getElementById(fragment);
                    if (el != null && this.model) {
                       this.scrollToMessages(el);
                   }
                 }
              });
            }).catch((err) => {
              console.error(err);
            });
        }
      });
    });
  }

  // moves the element, initiates the scroll, then moves it back - no visible "popping" if the element is already on the screen
  scrollToMessages(targetEle: HTMLElement){
    const pos = targetEle.style.position;
    const top = targetEle.style.top;
    targetEle.style.position = 'relative';
    // targetEle.style.top = '1500px';
    targetEle.scrollIntoView({behavior: 'smooth', block: 'start'});
    targetEle.style.top = top;
    targetEle.style.position = pos;
  }

  reload() {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('DiningRequestActions')
      .asQueryable(<DataServiceQueryParams>EditComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
      });
  }

  openAttachmentPreview(attachment: any) {
    return this._router.navigate([], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: false,
      queryParams: {
        download: attachment.url
      }
    });
  }

  closeAttachmentPreview(attachment: any) {
    return this._router.navigate([], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: false,
      queryParams: {
        download: null
      }
    });
  }

  download(attachment: any) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  rejectAttachment(attachment: any, templateToLoad: TemplateRef<any>) {
    this.attachmentToReject = attachment;
    this.modalRef = this._modalService.show(templateToLoad); 
  }

  // confirm callback of the reject attachment modal 
  // user accepted rejection => reject attachment and close modal
  public confirmRejectAttachmentModal( rejectionName: string ) {
    if (rejectionName && this.attachmentToReject !== null) {
      this.modalRef.hide();
      const headers = new Headers();
      const serviceHeaders = this._context.getService().getHeaders();
      // reject attachment
      const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/rejectAttachment`);
      this._loadingService.showLoading();
      return this._http.post(postUrl, this.attachmentToReject, {
        headers: serviceHeaders
      }).subscribe(async (result) => {
          if (result) {
            // notify user about reason of rejection by message                 
            const message = {
              subject: this._translateService.instant('UniversisDiningModule.RejectAttachmentSubject'),
              body: this._translateService.instant('UniversisDiningModule.RejectAttachmentBody', { attachmentName: this.attachmentToReject.attachmentType.name, rejectionName: rejectionName })
            };
            await this.sendWithoutAttachment(message);
            // clear 
            this.attachmentToReject = null;            
          }          
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/requests/dining']);
            });
          });
        }, (err) => {
          this._loadingService.hideLoading();
          // clear
          this.attachmentToReject = null;
          this._errorService.showError(err, {
            continueLink: '.'
          });      
        });               
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.RejectAttachmentModal.TitleOnUserError'));
    }
  }
  
  // cancel callback of the reject attachment modal 
  // user declined rejection => close modal
  public closeRejectAttachmentModal(): void {
    this.modalRef.hide();
  }

  revertAttachment(attachment: any) {
    // revert (accept back) attachment
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();    
    const postUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/revertAttachment`);
    this._loadingService.showLoading();
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(async (result) => {
        // reload page
        return this.reload()?.then(() => {
          this._loadingService.hideLoading();
          // send an application event
          this._appEvent.change.next({
            model: 'DiningRequestActions',
            target: this.model
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          const ReloadError = this._translateService.instant('Register.ReloadError') || {
            'Title': 'Refresh failed',
            'Message': 'The operation has been completed successfully but something went wrong during refresh.'
          };
          this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
            this._router.navigate(['/requests/dining']);
          });
        });
      }, (err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });      
      }
    );               
  }

  accept() {
    const AcceptConfirm = this._translateService.instant('UniversisDiningModule.AcceptConfirm') || {
      Title: 'Accept',
      Message: 'You are going to finally accept this application. Do you want to proceed?'
    };
    this._modal.showDialog(AcceptConfirm.Title, AcceptConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reject() {
    const RejectConfirm = this._translateService.instant('UniversisDiningModule.RejectConfirm') || {
      Title: 'Reject and cancel',
      Message: 'You are going to reject this application. Do you want to proceed?'
    };
    this._modal.showDialog(RejectConfirm.Title, RejectConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  revert() {
    const RevertConfirm = this._translateService.instant('UniversisDiningModule.RevertConfirm') || {
      Title: 'Activate application',
      Message: 'You are going to activate this application. Do you want to proceed?'
    };
    this._modal.showDialog(RevertConfirm.Title, RevertConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'ActiveActionStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reset() {
    const ResetConfirm = this._translateService.instant('UniversisDiningModule.ResetConfirm') || {
      Title: 'Change application status',
      // tslint:disable-next-line: max-line-length
      Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
    };
    this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {        
        this._loadingService.showLoading();                        
        // set effective status of potential request
        // const effectiveStatus = (this.model.attachments && this.model.attachments.some(x => x.published === false)) ? 'RejectedAttachmentsEffectiveStatus' : 'AcceptedAttachmentsEffectiveStatus';
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'PotentialActionStatus'
          },
          agent: null
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }


  onMessageEvent(event) {
    if (event.data && event.data.cancelMessage) {
      this.showNewMessage = false;
      this.newMessage = {};
    }
  }

  async sendWithoutAttachment(message) {
    try {
      this._loadingService.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        student: this.model.student,
        recipient: this.model.owner
      });
      await this._context.model(`DiningRequestActions/${this.model.id}/messages`).save(message);
      // reload message
      this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async send(message) {
    try {
      if (!(message.attachments && message.attachments.length)) {
        // send message without attachment
        await this.sendWithoutAttachment(message);
        return;
      }
      this._loadingService.showLoading();

      // set recipient (which is this action owner)
      Object.assign(message, {
        student: this.model.student.id,
        recipient: this.model.owner
      });

      const formData: FormData = new FormData();
      // get attachment if any
      if (message.attachments && message.attachments.length) {
        formData.append('attachment', message.attachments[0], message.attachments[0].name);
      }
      Object.keys(message).filter((key) => {
        return key !== 'attachments';
      }).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(message, key)) {
          formData.append(key, message[key]);
        }
      });
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const serviceUrl = this._context.getService().resolve(`DiningRequestActions/${this.model.id}/sendMessage`);
      await this._http.post(serviceUrl, formData, {
        headers: serviceHeaders
      }).toPromise();
      // reload message
      this.messages = await this._context.model(`DiningRequestActions/${this.model.id}/messages`)
        .asQueryable()
        .orderBy('dateCreated desc')
        .expand('attachments')
        .getItems();
      // clear message
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }

  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loadingService.showLoading();
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status != 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loadingService.hideLoading();
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

  acceptRequestAttachments(){
    const AcceptConfirmAttachments = this._translateService.instant('UniversisDiningModule.AcceptConfirmAttachments') || {
      Title: 'AcceptAttachments',
      Message: 'You are going to finally accept this applications attachments. Do you want to proceed?'
    };
    this._modal.showDialog(AcceptConfirmAttachments.Title, AcceptConfirmAttachments.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          effectiveStatus: {
            alternateName: 'AcceptedAttachmentsEffectiveStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  rejectRequestAttachments(){
    const RejectConfirmAttachments = this._translateService.instant('UniversisDiningModule.RejectConfirmAttachments') || {
      Title: 'RejectAttachments',
      Message: 'You are going to finally reject this applications attachments. Do you want to proceed?'
    };
    this._modal.showDialog(RejectConfirmAttachments.Title, RejectConfirmAttachments.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          effectiveStatus: {
            alternateName: 'RejectedAttachmentsEffectiveStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  invalidRequestData(){
    const InvalidRequestDataConfirm = this._translateService.instant('UniversisDiningModule.InvalidRequestDataConfirm') || {
      Title: 'The request data are invalid',
      Message: 'You are going to set this application s attached documents in Invalid request data state. Do you want to proceed?'
    };
    this._modal.showDialog(InvalidRequestDataConfirm.Title, InvalidRequestDataConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          effectiveStatus: {
            alternateName: 'InvalidRequestDataEffectiveStatus'
          }
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  setUnknownEffectiveStatus(){
    const unknownEffectiveStatusConfirm = this._translateService.instant('UniversisDiningModule.UnknownEffectiveStatusConfirm') || {
      Title: 'Unknown state',
      Message: 'You are going to set this application s attached documents in Unknown state. Do you want to proceed?'
    };
    this._modal.showDialog(unknownEffectiveStatusConfirm.Title, unknownEffectiveStatusConfirm.Message, DIALOG_BUTTONS.YesNo).then((result) => {
      if (result === 'yes') {
        this._loadingService.showLoading();
        this._context.model('DiningRequestActions').save({
          id: this.model.id,
          effectiveStatus: null
        }).then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loadingService.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
          }).catch((err) => {
            this._loadingService.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }


  public claim() {
    if (this.model) {
      this._loadingService.showLoading();
      return this._userService.getUser().then((user) => {
        const item = {
          id: this.model.id,
          agent: {
            id: user.id,
            name: user.name
          }
        };
        return this._context.model('DiningRequestActions').save(item).then(() => {
          return this.reload()?.then(() => {
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
            this._loadingService.hideLoading();
          });
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }

  public release() {
    if (this.model) {
      this._loadingService.showLoading();
      return this._userService.getUser().then((user) => {
        const item = {
          id: this.model.id,
          agent: null
        };
        return this._context.model('DiningRequestActions').save(item).then(() => {
          return this.reload()?.then(() => {
            this._appEvent.change.next({
              model: 'DiningRequestActions',
              target: this.model
            });
            this._loadingService.hideLoading();
          });
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }

}
