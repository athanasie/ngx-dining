import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { RouterModalOkCancel, RouterModal } from '@universis/common/routing';
import { ActivatedRoute, Router } from '@angular/router';
import { AppEventService } from '@universis/common';
import { ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { AdvancedFormComponent } from '@universis/forms';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'dining-modal-edit-action',
  templateUrl: './modal-edit.component.html',
  styles: [`
  .modal-dialog-100 {
    position: absolute;
    margin-top: 0rem;
    max-width: 100%;
    width: 100%;
  }
  .modal-dialog.modal-dialog-100 .modal-header button.close {
      top: -2rem;
      color: #3e515b;
      right: -1.5rem;
    }
  .modal-dialog-100 .modal-content {
        border-radius: 0px;
      }
  `],
  encapsulation: ViewEncapsulation.None
})
export class ModalEditComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  dataSubscription: Subscription;
  public showActions = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _translateService: TranslateService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-dialog-100';
    this.modalTitle = this._translateService.instant('Settings.EditItem') + ' ('
      + this._translateService.instant('NewRequestTemplates.DiningRequestAction.Name') + ')';
    this.dataSubscription = this.activatedRoute.data.subscribe((data) => {
      if (data['action'] === 'modal-edit') {
        this.showActions = true;
      }
    });
  }

  ngOnInit() {
    this.cancelButtonClass = 'd-none';
    this.okButtonText = this._translateService.instant('Reports.Viewer.Close');
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ok(): Promise<any> {
    return super.close();
  }

}

