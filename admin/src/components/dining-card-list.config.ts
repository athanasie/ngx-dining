// tslint:disable: quotemark
export const DiningCardListConfiguration = {
  "title": "DiningCards",
  "model": "StudentDiningCards",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(action/requestNumber, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${id}", "edit"]
            }
          }
        ]
      }
    },
    {
      "name": "action/requestNumber",
      "property":"requestNumber",
      "title": "Requests.RequestNumber",
      "filter": "(indexof(action/requestNumber, '${value}') ge 0)",
      "type": "text",
    },
    {
      "name": "action/actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'"
            }
          }
        }
      ]
    }, 
    {
      "name": "action/code",
      "property": "code",
      "title": "Requests.RequestCode",
      "hidden": true
    },   
    {
      "name": "active",
      "title": "UniversisDiningModule.DiningCardsTemplates.active",
      "show": true,
      "type": "boolean",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "UniversisDiningModule.DiningCardStatuses.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${active}'==='false'",
              "text-success": "'${active}'==='true'"
            }
          }
        }
      ]
    },
    {
      "name": "academicYear",
      "property": "academicYear",
      "title": "UniversisDiningModule.DiningCardsTemplates.AcademicYear",
      "formatter": "TemplateFormatter",
      "hidden": true
    },
    {
      "name": "academicPeriod",
      "property": "academicPeriod",
      "title": "UniversisDiningModule.DiningCardsTemplates.AcademicPeriod",
      "formatter": "TemplateFormatter",
      "hidden": true
    },
    {
      "name": "validFrom",
      "title": "UniversisDiningModule.DiningCardsTemplates.validFrom",
      "filter": "(validFrom eq '${value}')",
      "type": "text",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },
    {
      "name": "validThrough",
      "title": "UniversisDiningModule.DiningCardsTemplates.validThrough",
      "filter": "(validThrough eq '${value}')",
      "type": "text",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },

    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Students.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Students.GivenName",
      "hidden": true
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "student/department/name",
      "property": "studentDepartmentName",
      "title": "Students.Department",
    },

    {
      "name": "student/uniqueIdentifier",
      "property": "studentUniqueIdentifier",
      "title": "Requests.StudentUniqueIdentifier"
    },
    {
      "name": "student",
      "property": "student",
      "title": "Students.StudentIdentifier",
      "formatter": "TemplateFormatter",
      "hidden": true
    }
  ],
  "criteria": [
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/id eq 1)' : '(student/studentStatus/id ne 1)'}", 
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(indexof(student/department/name,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "academicYear",
      "filter": "(indexof(academicYear/alternateName,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "academicPeriod",
      "filter": "(indexof(academicPeriod/alternateName,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "validFrom",
      "filter": "(validFrom ge '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "validThrough",
      "filter": "(validThrough le '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "active",
      "filter": "(active eq '${value}')",
      "type": "text"
    },
    {
      "name": "location",
      "filter": "(location eq '${value}')",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(action/requestNumber, '${value}') ge 0)",
      "type": "text"
    },
  ],
  "searches": []
};


