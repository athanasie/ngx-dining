import {Component, Input, OnInit} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'dining-student-side-preview',
  templateUrl: './side-preview.component.html'
})
export class SidePreviewComponent implements OnInit {

  paramSubscription: any;

  @Input() model;
  public currentLang: any;

  constructor(private _configurationService: ConfigurationService, 
       private _context: AngularDataContext) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
  }
}
