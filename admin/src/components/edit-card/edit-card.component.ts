import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, Input, OnInit, DoCheck, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { ModalService, LoadingService, ErrorService, AppEventService, DIALOG_BUTTONS } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'lib-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss', '../../../../src/lib/dining.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditCardComponent implements OnInit, AfterViewInit {
  public static readonly ServiceQueryParams = {
    $expand: 'student($expand=person,studentStatus,inscriptionMode,department,studyProgram($expand=studyLevel)), action($expand=agent,review,effectiveStatus,actionStatus, modifiedBy)'
  };

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form?: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public modalRef!: BsModalRef;
  cancelReason: any = null;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _modalService: BsModalService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService) { }


  ngOnInit() {
//
  }

  ngAfterViewInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      //
    });
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this.model = data['model'];
    });
    if (this.form) {
      this.form.refreshForm.emit({
        submission: {
          data: this.model
        }
      });
    }
  }

  reload() {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('StudentDiningCards')
      .asQueryable(<DataServiceQueryParams>EditCardComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
      });
  }


  cancel(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  reactivate(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }


  // confirm callback of the cancel card modal 
  // user accepted to cancel dining card => reject active state and reset cancelReason and close modal
  public confirmCancelCardModal(cancelReason: string) {
    if (cancelReason) {
      this._loading.showLoading();
      this.modalRef.hide();
      // cancel dining card
      this.model.active = false;
      // set today as dateCancelled
      this.model.dateCancelled = this.model.validThrough = new Date;
      this.model.cancelReason = cancelReason;
      return this._context.model('StudentDiningCards').save(this.model)
        .then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudentDiningCards',
              target: this.model
            });
            this.cancelReason = null;
            this.ngAfterViewInit();
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }, (err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.CancelCardModal.TitleOnUserError'));
    }
  }

  confirmCancelCardActiveRequestModal(cancelReason: string){
    if (cancelReason) {
      this._loading.showLoading();
      this.modalRef.hide();
      // cancel dining card
      return this._context.model(`StudentDiningCards/${this.model.id}/Cancel`).save({
        cancelReason: cancelReason
      })
        .then(() => {
          // reload page
          return this.reload()?.then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudentDiningCards',
              target: this.model
            });
            this.cancelReason = null;
            this.ngAfterViewInit();
          }).catch((err) => {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              'Title': 'Refresh failed',
              'Message': 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/dining']);
            });
          });
        }, (err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.CancelCardModal.TitleOnUserError'));
    }
  }

  public confirmReactivateCardModal() {
    this.model.active = true;
    this.checkDates();
    this.modalRef.hide();
    // cancel dining card
    this._loading.showLoading();
    return this._context.model('StudentDiningCards').save(this.model).then(async (result) => {
      this.model = result;
      // reload page
      return this.reload()?.then(() => {
        // send an application event
        this._appEvent.change.next({
          model: 'StudentDiningCards',
          target: this.model
        });
        this.ngAfterViewInit();
      }).catch((err) => {
        this._loading.hideLoading();
        const ReloadError = this._translateService.instant('Register.ReloadError') || {
          'Title': 'Refresh failed',
          'Message': 'The operation has been completed successfully but something went wrong during refresh.'
        };
        this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
          this._router.navigate(['/requests/dining']);
        });
      });
    }, (err) => {
      this._loading.hideLoading();
      // clear
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  checkDates(){
    if (typeof this.model.dateCancelled !== 'undefined' && this.model.dateCancelled != null) {
      this.model.dateCancelled = null;
    }
    if (typeof this.model.cancelReason != 'undefined' && this.model.cancelReason != null) {
      this.model.cancelReason = null;
    }
  }

  public confirmUndoCardModal() {
    this.model.active = false;
    this.modalRef.hide();
    // cancel dining card
    this._loading.showLoading();
    return this._context.model('StudentDiningCards').save(this.model)
      .then((result) => {
        this.model = result;

        // reload page
        return this.reload()?.then(() => {

          this._loading.hideLoading();
          // send an application event
          this._appEvent.change.next({
            model: 'StudentDiningCards',
            target: this.model
          });
        }).catch((err) => {
          this._loading.hideLoading();
          const ReloadError = this._translateService.instant('Register.ReloadError') || {
            'Title': 'Refresh failed',
            'Message': 'The operation has been completed successfully but something went wrong during refresh.'
          };
          this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
            this._router.navigate(['/requests/dining']);
          });
        });
      }, (err) => {
        this._loading.hideLoading();
        // clear
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

}
