
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  selector: 'lib-modal-edit-card',
  templateUrl: './modal-edit-card.component.html',
  styleUrls: ['./modal-edit-card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ModalEditCardComponent extends RouterModalOkCancel implements OnInit  {
  dataSubscription: Subscription;
  public showActions = false;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  constructor(router: Router,
    activatedRoute: ActivatedRoute,
    private _translateService: TranslateService) {
    super(router, activatedRoute);

    // set modal size
    this.modalClass = 'modal-dialog-100';
    this.modalTitle = this._translateService.instant('Settings.EditItem') + ' ('
      + this._translateService.instant('UniversisDiningModule.DiningCardsTemplates.DiningCard.Name') + ')';
    this.dataSubscription = this.activatedRoute.data.subscribe((data) => {
      if (data['action'] === 'modal-edit') {
        this.showActions = true;
      }
    });
  }

  ngOnInit() {
    this.cancelButtonClass = 'd-none';
    this.okButtonText = this._translateService.instant('Reports.Viewer.Close');
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ok(): Promise<any> {
    return super.close();
  }

}

