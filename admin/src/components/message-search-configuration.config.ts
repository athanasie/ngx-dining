export const MessageSearchConfiguration = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Requests.RequestNumber",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "requestNumber",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 2,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.Messages.SentByStudent",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "sentByStudent",
              "input": true,
              "hideOnChildrenHidden": false,
              "widget": "choicesjs",
              "type": "select",
              "data": {
                "values": [
                  {
                    "value": "DiningUsers",
                    "label": "UniversisDiningModule.Yes"
                  }
                ]
              },
              "dataType": "boolean",
            }
          ],
          "width": 2,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.Messages.Sender",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "senderName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.Messages.Recipient",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "recipientName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.ActionStatusTitle",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "ActionStatusTypes",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.alternateName }}",
              "key": "actionStatus",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.Messages.SentAfter",
              "tableView": false,
              "enableTime": false,
              "enableMinDateInput": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "minDate",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "locale": "en",
                "useLocaleSettings": false,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "format": "yyyy-MM-dd",
                "hourIncrement": 1,
                "minuteIncrement": 1,
                "time_24hr": true,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "UniversisDiningModule.Messages.SentBefore",
              "tableView": false,
              "enableMinDateInput": false,
              "enableTime": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "maxDate",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "useLocaleSettings": true,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "time_24hr": false,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.EffectiveStatus",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "DiningRequestEffectiveStatusTypes",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.alternateName }}",
              "key": "effectiveStatus",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.StudentStatus",
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentActiveStatus",
              "input": true,
              "searchEnabled": false,
              "hideOnChildrenHidden": false,
              "widget": "choicesjs",
              "type": "select",
              "selectThreshold": 0.3,
              "valueProperty": "value",
              "selectValues": "value",
              "data": {
                "values": [
                  {
                    "value": 1,
                    "label": "StudentStatuses.active"
                  },
                  {
                    "value": -1,
                    "label": "StudentStatuses.NotActive"
                  }
                ]
              },
              "dataType": "boolean",
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.Edit.claim",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "DiningRequestActions?$select=agent/name as name&$groupby=agent/name&$orderby=agent/name&$filter=agent ne null",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.name }}",
              "key": "agent",
              "valueProperty": "name",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": true
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.StudentDepartment",
              "labelPosition": "top",
              "widget": "choicesjs",
              "key": "studentDepartment",
              "dataSrc": "url",
              "data": {
                "url": "LocalDepartments?$top=-1&$skip=0&$orderby=name",
                "headers": []
              },
              "template": " {{item.id}} - {{item.name}} ",
              "selectValues": "value",
              "valueProperty": "name",
              "type": "select",
              "input": true,
              "disabled": false,
              "lazyLoad": false,
              "searchEnabled": true
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
