import {Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ErrorService, LoadingService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {NgxExtendedPdfViewerComponent} from 'ngx-extended-pdf-viewer';
import {AppEventService} from '@universis/common';
import {EventEmitter} from '@angular/core'

declare var $: any;

@Component({
  selector: 'dining-attachment-download',
  templateUrl: './attachment-download.component.html',
  styleUrls: ['./attachment-download.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AttachmentDownloadComponent implements OnInit, OnDestroy {

  @ViewChild('pdfViewer') pdfViewer?: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer?: ElementRef<HTMLDivElement>;

  private changeSubscription: Subscription | undefined;
  public recordsTotal: number | undefined;
  public blob: any;
  public currentItem: any;
  @Output() close: EventEmitter<void> = new EventEmitter();
  @Output() load: EventEmitter<void> = new EventEmitter();

  constructor(private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _appEvent: AppEventService,
              private _loadingService: LoadingService) {
  }

  ngOnInit() {
    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      // if change event refers to documents
      if (event && event.target && event.target.url != null && event.model === 'Attachments') {
        // check target id
        if (this.currentItem && event.target.url === this.currentItem.url) {
          this.show(event.target);
        }
      }
    });
  }

  show(item: { url: string }) {
    if (/^\//.test(item.url) === false) {
      return this._errorService.showError(new Error('Expected a relative URL.'), {continueLink: '.'});
    }
    this._loadingService.showLoading();
    setTimeout(() => {
      this.currentItem = item;
      this.download(item.url).then((result) => {
        this.blob = result;
        this.showViewer();
        // hide body overflow
        $(document.body).css('overflow-y', 'hidden');
        this._loadingService.hideLoading();
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {continueLink: '.'});
      });
    }, 100);
  }

  hide() {
    $(this.pdfViewerContainer?.nativeElement).hide();
    $(document.body).css('overflow-y', 'unset');
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(attachmentURL: any) {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    this.currentItem = {
      url: attachmentURL
    };
    const url = this.currentItem.url;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context.getService().resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.ok === false) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    });
  }

  ngOnDestroy() {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  showViewer() {
    $(this.pdfViewerContainer?.nativeElement).show();
    this.pdfViewer?.onResize();
    // add close button
    const toolbarViewerRight = $(this.pdfViewerContainer?.nativeElement)
      .find('#toolbarViewerRight');
    if (toolbarViewerRight.find('pdf-close-tool').length === 0) {
      const closeText = this._translateService.instant('Reports.Viewer.Close');
      const closeTool = $(`
        <pdf-close-tool>
            <button style="width:auto" class="toolbarButton px-2" type="button">
                ${closeText}
            </button>
        </pdf-close-tool>
      `);
      // prepare to close viewer
      closeTool.find('button').on('click', () => {
        $(this.pdfViewerContainer?.nativeElement).hide();
      });
      // insert before first which is the pdf hand tool
      closeTool.insertBefore(toolbarViewerRight.find('pdf-hand-tool'));
    }
  }

  closeViewer() {
    if (this.close != null) {
      return this.close.emit();
    }
    $(this.pdfViewerContainer?.nativeElement).hide();
    $(document.body).css('overflow-y', 'unset');
  }

  pdfClose() {
    this.closeViewer();
  }

  printAction() {
    $(this.pdfViewerContainer?.nativeElement).find('pdf-print>button').trigger('click');
  }

  downloadAction() {
    if (this.pdfViewer) {
      this.pdfViewer.filenameForDownload = this.currentItem.name;
      this.pdfViewer.ngOnChanges({
        filenameForDownload: this.currentItem.name
      });
    }
    $(this.pdfViewerContainer?.nativeElement).find('pdf-download>button').trigger('click');
  }


}
