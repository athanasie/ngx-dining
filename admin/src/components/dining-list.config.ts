// tslint:disable: quotemark
export const DiningListConfiguration = {
  "title": "Requests.ActiveStudentRequestActions",
  "model": "DiningRequestActions",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(requestNumber eq  '${text}' or indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0)",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${id}", "edit"]
            }
          }
        ]
      }
    },
    {
      "name": "requestNumber",
      "property": "requestNumber",
      "title": "Requests.RequestNumber"
    },
    {
      "name": "startTime",
      "property": "startTime",
      "title": "Requests.StartTime",
      "formatter": "DateTimeFormatter",
      "formatString": "shortDate"
    },
    {
      "name": "effectiveStatus/alternateName",
      "property": "effectiveStatus",
      "title": "Requests.EffectiveStatus",
      "className": "text-center",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${effectiveStatus == null ? 'UnknownStatus' : effectiveStatus}"
        },
        {
          "formatter": "TranslationFormatter",
          "formatString": "EffectiveStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${effectiveStatus}' === 'RejectedAttachmentsEffectiveStatus' || '${effectiveStatus}' === 'InvalidRequestDataEffectiveStatus'",
              "text-success": "'${effectiveStatus}' === 'AcceptedAttachmentsEffectiveStatus'"
            }
          }
        },
      ]
    },
    {
      "name": "actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'"
            }
          }
        }
      ]
    },
    {
      "name": "agent/name",
      "property": "agent",
      "title": "Requests.Edit.claim"
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Students.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Students.GivenName",
      "hidden": true
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "StudentStatuses.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-success": "'${studentStatus}'==='active'",
              "text-danger": "'${studentStatus}'!=='active'"
            }
          }
        }
      ]
    },
    {
      "name": "student/department/name",
      "property": "studentDepartment",
      "title": "Students.Department",
    },
    {
      "name": "student/uniqueIdentifier",
      "property": "studentUniqueIdentifier",
      "title": "Requests.StudentUniqueIdentifier"
    },
    {
      "name": "student/id",
      "property": "studentId",
      "title": "Student.Id",
      "hidden": true
    },
    {
      "name": "owner",
      "property": "owner",
      "title": "Owner",
      "hidden": true
    }
  ],
  "defaults": {
    "orderBy": "dateCreated desc"
  },
  "paths": [
    {
      "name": "Requests.Active",
      "show": true,
      "alternateName": "list/active",
      "filter": {
      }
    },
  ],
  "criteria": [
    {
      "name": "lessThan25yrs",
      "filter": "(lessThan25yrs eq ${value})",
      "type": "text"
    },
    {
      "name": "livePermanentlyInSameLocationWithInstitution",
      // tslint:disable-next-line: max-line-length
      "filter": "(institutionOrClubLocationSameAsStudentPermanentResidence eq ${value} or institutionOrClubLocationSameAsGuardianPermanentResidence eq ${value})",
      "type": "text"
    },
    {
      "name": "foreignScholarStudent",
      "filter": "(foreignScholarStudent eq ${value})",
      "type": "text"
    },
    {
      "name": "diningPrerequisites",
      // tslint:disable-next-line: max-line-length
      "filter": "(militaryActive ne ${value} and militarySchool ne ${value} and studentHouseResident ne ${value} and sameDegreeHolder ne ${value})",
      "type": "text"
    },
    {
      "name": "unemployment",
      // tslint:disable-next-line: max-line-length
      "filter": "(studentSpouseReceivesUnemploymentBenefit eq ${value} or studentGuardianReceivesUnemploymentBenefit eq ${value} or studentReceivesUnemploymentBenefit eq ${value})",
      "type": "text"
    },
    {
      "name": "studentMaritalStatus",
      "filter": "(maritalStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "additionalType",
      "filter": "(additionalType eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/id eq 1)' : '(student/studentStatus/id ne 1)'}", 
      "type": "text"
    },
    {
      "name": "actionStatus",
      "filter": "(actionStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "effectiveStatus",
      "filter": "(effectiveStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(requestNumber, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "startTime",
      "filter": "(startTime eq '${value}')",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')"
    },
    {
      "name": "maxDate",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')"
    },
    {
      "name": "agent",
      "filter": "(agent/name eq '${value}')",
      "type": "text"
    },
    {
      "name": "me",
      "filter": "(agent/name eq me())",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(indexof(student/department/name,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "requestIsClaimed",
      "filter": "${value >=1 ? '(agent ne null)' : '(agent eq null)'}", 
      "type": "text"
    },
    {
      "name": "effectiveStatusRating",
      "filter": "${value >=1 ? '(effectiveStatus/alternateName ne null)' : '(effectiveStatus/alternateName eq null)'}", 
      "type": "text"
    },
  ],
  "searches": []
};