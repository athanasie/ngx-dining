import { NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { DiningComponent } from './dining.component';
import { DiningRoutingModule } from './dining-routing.module';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { DiningSharedModule } from '@universis/ngx-dining/shared';
import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DiningRoutingModule,
    DiningSharedModule,
    FormsModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    TabsModule.forRoot(),
    NgArrayPipesModule,
    NgxDropzoneModule
  ],
  declarations: [DiningComponent, ComposeMessageComponent, SendMessageActionComponent],
  exports: [DiningComponent]
})
export class DiningModule { //implements OnInit {
  constructor() {
  }
}