import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Injectable()
export class DiningConfigurationService {

  constructor() { }
}

@Injectable()
export class DiningConfigurationResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) {
   }

  resolve(): Promise<any> | any {
    return this._context.model('DiningConfigurations')
      .asQueryable()
      .orderBy('id')
      .getItems().then(res => {
        if (res) {
          return Promise.resolve(res[0]);
        }
        return Promise.resolve(null)
      });
  }

}