import { NgModule } from "@angular/core";
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { DiningConfigurationResolver } from "./dining-configuration.service";
import { DINING_LOCALES } from './i18n/index';

@NgModule({
    declarations: [],
    imports: [
        TranslateModule
    ],
    exports: [],
    providers:[
        DiningConfigurationResolver
    ]
})

export class DiningSharedModule {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.log('An error occurred while loading dining shared module');
            console.error(err);
        });
    }  

    static forRoot() {
        return {
            ngModule: DiningSharedModule
        };
    }

    async ngOnInit() {
        Object.keys(DINING_LOCALES).forEach(lang => {
            if (DINING_LOCALES.hasOwnProperty(lang)) {
            this._translateService.setTranslation(lang, DINING_LOCALES[lang], true);
            }
        });
    }
}