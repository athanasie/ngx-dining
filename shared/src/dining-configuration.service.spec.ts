import { TestBed } from '@angular/core/testing';

import { DiningConfigurationService } from './dining-configuration.service';

describe('DiningConfigurationService', () => {
  let service: DiningConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiningConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
