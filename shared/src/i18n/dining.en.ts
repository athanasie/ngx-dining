/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const en = {
  NewRequestTemplates: {
    DiningRequestAction: {
      "Name": "Dining Request",
      "Title": "Dining Requests",
      "Summary": "Dining Request",
      "Description": "Dining Requests",
      "History": "History"
    },
    "NewDiningRequest":"New" 
  },
  "Settings": {
    "EditItem": "Edit"
  },
  "StudentStatuses": {
    "active": "Active",
    "candidate": "Candidate",
    "declared": "Declared",
    "erased": "Erased",
    "graduated": "Graduated",
    "null": "-",
    "suspended": "Suspended",
    "NotActive": "Not Active"
  },
  "Requests": {
    "ActionStatusTitle": "Request Status",
    "StartTime": "Request date",
    "StudentUniqueIdentifier": "Student Unique Identifier",
    "StudentStatus": "Student status",
    "RequestedAfter": "Requested after",
    "RequestedBefore": "Requested before",
    "FatherName": "Father name",
    "MotherName": "Mother name",
    "RequestNumber": "Request Number",
    "GivenName": "Given name",
    "EffectiveStatus": "Control State",
    "EffectiveStatusRating": "Effective Status Rating",
    "RequestCode": "Request code",
    "Edit": {
      "release": "Release",
      "claim": "Claim action",
      "NotClaimed": "This action has not be claimed by someone user. Press [Claim action] to complete this request.",
      "CompletedWithErrors": {
        "Title": "Completed with errors",
        "Description": {
          "One": "The message failed to send due to an error occurred while executing process.",
          "Many": "{{errors}} messages failed to send due to an error occurred while executing process."
        }
      }
    },
    "Claimed": "Claimed"
  },
  "ActionStatusTypes": {
    "ActiveActionStatus": "Active",
    "CancelledActionStatus": "Cancelled",
    "CompletedActionStatus": "Completed",
    "FailedActionStatus": "Failed",
    "PotentialActionStatus": "Potential",
    "null": "-"
  },
  "EffectiveStatusTypes": {
    "RejectedAttachmentsEffectiveStatus": "Non valid documents",
    "AcceptedAttachmentsEffectiveStatus": "Valid documents",
    "UnknownStatus": "",
    "InvalidRequestDataEffectiveStatus": "Invalid request data"
  },
  "DiningCardStatuses": {
    "Active": "Active",
    "Cancelled": "Cancelled",
  },
  "Reports": {
    "Viewer": {
      "Close": "Close",
      "Download": "Download file",
      "Print": "Print file",
      "Sign": "Sign document"
    },
  },
  "Students": {
    "FamilyName": "Family name",
    "GivenName": "Given name",
    "StudentIdentifier":"Student number",
    "StudentUniqueIdentifier": "Student identifier",
    "FullName": "Full name",
    "PersonalDetails": "Personal details",
    "StatusTypes": {
      "active": "Active",
      "candidate": "Candidate",
      "declared": "Graduated",
      "erased": "Erased",
      "graduated": "Graduated",
      "suspended": "Suspended"
    },
    "InscriptionModeCategory": "Inscription mode category",
    "Department": "Department name",
    "InscriptionYear": "Inscription year",
    "Semester": "Semester",
    "BirthDate": "Birth date",
    "BirthPlace": "Birth Place",
    "Gender": "Gender",
    "Nationality": "Citizenship",
    "VatNumber": "Vat number",
    "DepartmentAbbreviation": "Department name",
    "StudentDepartment": "Department name",
    "StudentCategory": "Student category",
    "StudentTitleSingular": "name",
    "FatherName": "Father name",
    "MotherName": "Mother name",
    "ContactDetails":"Contact Details",
    "phone": "Phone",
    "email": "email", 
    "StudyLevel": "Study level",
    "StudyLevels": {
      "undergraduate": "Undergraduate",
      "postgraduate": "Postgraduate",
      "specialprogram": "Special Program",
      "doctoral": "Doctoral",
      "postdoctoral": "Postdoctoral"
    },
  },
  "Register": {
    "Accept": "Accept",
    "Attachments": "Attachments",
    "DateCreated": "Created at",
    "DateModified": "Modified at",
    "DateSubmitted": "Submitted at",
    "Details": "Details",
    "Download": "Download",
    "Message": "Message",
    "Messages": "Messages",
    "NewMessage": "Compose new message",
    "NoAttachments": "No attachments yet",
    "NoMessages": "No messages yet",
    "Preview": "Preview",
    "Reject": "Reject",
    "Reset": "Reset application to pending",
    "Revert": "Revert to active",
    "Review": "Review",
    "TotalIncomeBasedOnStudentsRequest": "Total family income based on student's request",
    "SendMessage": "Send a message",
    "Status": "Status",
    "UploadFileHelp": "Drop file to attach, or browse",
    "UploadFilesHelp": "Drop files to attach, or browse",
    "RequestNumber": "Request number",
    "ComposeNewMessage": {
      "Title": "Compose new message",
      "Description": "The operation will try to send a message to the candidates of the selected applications. Please write a short message below and start sending messages.",
      "Subject": "Subject",
      "WriteMessage": "Write a short message",
      "Send": "Send",
      "Cancel": "Cancel",
    },
    "UserReviewAdded": "User Review Added",
    "addReview": "Add Review",
    "Edit": "Edit",
    "NoReview": "No Review",
    "Rating": "Rating",
    "CalculatedTotalFamilyIncome": "Calculated total family income" ,
    "CheckingLimit": "Checking limit",
    "NumberOfSiblingStudents": "Number of sibling students",
    "CodeTaxValue": "The field 003 value of tax clearance document",
    "TotalFamilyIncome": "Total family income",
    "RequestDetails": "Dining request details"
  },
  "Forms": {
    "DiningRequest": {
      "Disabled": "Disabled",
      "LessThan25yrs": "Less than 25yrs old",
      "LivePermanentlyInSameLocationWithInstitution": "Student Lives Permanently In Same Location With Institution",
      "ForeignScholarStudent": "Foreign Scholar Student",
      "Prerequisites": "Prerequisites",
      "Unemployment": "Student Receives Unemployment Benefit",
      "StudentMaritalStatusTitle": "Marital Status",
    },
    "DiningRequestAction": "Dining Request",
    "active": "Active",
    "candidate": "Candidate",
    "declared": "Declared",
    "erased": "Erased",
    "graduated": "Graduated",
    "null": "-",
    "suspended": "Suspended",
    "transfered":"Transfered",
    "single": "Single",
    "married": "Married",
    "RejectedAttachmentsEffectiveStatus": "Non valid documents",
    "AcceptedAttachmentsEffectiveStatus": "Valid documents",
    "InvalidRequestDataEffectiveStatus": "Invalid Request",
  },
  "summer": "Summer",
  "winter": "Winter",
  "AcademicPeriod": {
    "summer": "Summer",
    "winter": "Winter"
  },
  "AcademicYear": "Academic Year",
  "Attachments":{
    "Accept":"Valid documents",
    "Reject": "Non valid documents",
    "UnknownStatus": "Unknown",
    "InvalidRequestData": "Invalid request data"
  },
  "StudyLevel": "Study level",
  UniversisDiningModule: {
    DiningCardsTemplates: {
      DiningCard: {
        Name: 'Dining Card',
        Title: 'Dining Card',
        Summary: 'Dining Card',
        Description: 'Dining Cards',
      },
      validFrom: 'Valid from',
      validThrough: 'Valid through',
      SerialNumber: 'Card No',
      active: 'Dining card status',
      CancelItem: 'Suspend',
      ReactivateItem: 'Reactivate',
      AcademicYear: 'Academic year',
      AcademicPeriod: 'Academic period',
    },
    DiningCardStatuses: {
      true: "Active",
      false: "Suspended",
    },
    AcademicPeriod: {
      summer: "summer",
      winter: "winter"
    },
    summer: "Summer",
    PersonalInformation: 'Personal Information',
    DiningDocuments: 'Documents',
    DiningRequestTitle: 'Dining request',
    DiningRequestCapitalTitle: 'DINING REQUEST',
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your dining request.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for dining is ongoing',
        completed: 'Document submission for dining is completed',
        failed: 'Document submission for dining is ongoing',
        unavailable: 'Document submission is not available'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      DiningDocumentToUpload: 'document for upload',
      DiningDocumentsToUpload: 'documents for upload',
      DiningDocumentsPhysicals: 'documents to be delivered to the secretariat of the department',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      },
    StudentDiningCardExists: "Student Dining Card Exists", 
    },
    ModalConfirm: {
      Submit: 'Submit',
      Close: 'Close',
      Title: 'Send Dining Request',
      Body: 'You want to send your request for sending and check to the secretariat?'
    },
    Messages: {
      Title: 'Messages',
      NewMessage: 'New message',
      NoSubject: 'No subject',
      Subject: 'Subject',
      WriteMessage: 'Your message',
      IncomingMessage: 'Incoming message',
      NoMessages: 'No messages',
      Info:"To send supporting documents, click \"New message\" and attach the necessary documents",
      SentAfter: "Sent after",
      SentBefore: "Sent before",
      Sender: "Sender",
      Recipient: "Recipient",
      SentByStudent: "Sent by student",
      MarkAsRead: "Mark as read",
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    Time: 'Time',
    Location: 'Location',
    Download: 'Download',
    Previous: 'Previous',
    Next: 'Next',
    Submit: 'Submit',
    Completed: 'Request Completion',
    ContactRegistrar: 'Contact Registrar',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty: 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Dining Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Semester',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group',
    StatusLabel: 'Status of your request',
    NoAttachments: 'There are no attachments for upload',
    TemporarySaveMessage: 'Dining request has been saved temporarily',
    AttachDocumentMessage: 'Please attach all required documents',
    RequestPeriodExpired: 'The period for dining requests has expired.',
    RequestPeriodNotStarted: 'Dining request period will be open from {{dateStart}} to {{dateEnd}}.',
    NoDiningRequestEvent: 'No dining request period defined.',
    EmptyDocumentList: "The list of required documents is empty.",
    EmptyDocumentListContinue: "The list of required documents is empty. You can continue by submitting your application.",
    VatNumberCrosscheckFailed: "Caution. The VAT check failed",
    OutΟf:"out of",
    InformationIsNotAvailable: "Information is not available",
    ModifiedBy: "Modified by",
    AcceptedAt: "Accepted at",
    "AcceptConfirm": {
      "Title": "Accept and complete",
      "Message": "You are going to accept this application. After this operation the candidate will have a new student dining card. Do you want to proceed?"
    },
    "RejectConfirm": {
      "Title": "Reject application",
      "Message": "You are going to reject this application. The candidate will be informed about application rejection. Do you want to proceed?"
    },

    "AcceptAction": {
      "Title": "Accept requests",
      "Description":"The operation will try to complete the selected requests. All the pre-defined procedures for each type of request will be performed eg document numbering or archiving. The process is valid for pending requests only."
    },
    "RejectAction": {
      "Title": "Reject requests",
      "Description":"The operation will try to reject the selected requests. Request owners will be informed for this operation by the system interfaces. The process is valid for pending requests only."
    },
    "AcceptConfirmAttachments": {
      "Title": "Accept request's attached documents",
      "Message": "You are going to accept this request's attached documents. Do you want to proceed?"
    },
    "RejectConfirmAttachments": {
      "Title": "Reject request's attached documents",
      "Message": "You are going to reject this request's attached documents. Do you want to proceed?"
    },
    "InvalidRequestDataConfirm": {
      "Title": "The request data are invalid",
      "Message": "You are going to set this application's attached documents in 'Invalid request data' state. Do you want to proceed?"
    },
    "UnknownEffectiveStatusConfirm": {
      "Title": "Unknown state",
      "Message": "You are going to set this application's attached documents in 'Unknown' state. Do you want to proceed?"
    },
    "ResetConfirm": {
      "Title": "Change application status",
      "Message": "You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?"
    },
    "RevertConfirm": {
      "Title": "Activate application",
      "Message": "You are going to activate this application again. Do you want to proceed?"
    },
    "SuspendAction": {
      "Title": "Suspend cards",
      "Description":"The operation will try to suspend the selected dining cards. All the pre-defined procedures for each type of dining card will be performed eg document numbering or archiving. The process is valid for active dining cards only."
    },
    "ReactivateAction": {
      "Title": "Reactivate cards",
      "Description":"The operation will try to reactivate the selected dining cards. All the pre-defined procedures for each type of dining card will be performed eg document numbering or archiving. The process is valid for suspended dining cards only."
    },
    CancelCardModal: {
      "ResetConfirm": {
        "Title": "Suspend dining card",
        "Message": "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Άκυρο'. If you also want to revert the dining request in 'Active' state then you have to click 'Suspend dining card and revert dining request'. The candidate will be notified by message. Do you want to proceed?"
      },
      Title: "Suspend",
      Help: "You are going to set this dining card in suspended state. If you also want to revert the dining request in 'Active' state then you have to click 'Suspend dining card and revert dining request'. The candidate will be notified by message. Do you want to proceed?",
      CancelNamePlaceholder: "Enter suspension reason",
      Accept: "Suspension",
      CancelCardActiveRequest: "Suspend and revert", 
      Close: "Close",
      TitleOnUserError: "You must specify the reason of suspension."
    },
    ReactivateCardModal:{
      "ResetConfirm": {
        "Title": "Change dining card status",
        "Message": "You are going to set this dining card in active state.  Do you want to proceed?"
       },
      Title: "Reactivate",
      Accept: "Reactivation",
      Close: "Close",
    },
    CancelCardSubject: "Suspension of dining card",
    CancelCardBody: "The dining card was suspended due: {{cancelReason}}",
    RevertCard: "Revert",
    RejectAttachmentModal: {
      Title: "Reject document",
      Help: "Specify the reason of rejection. The candidate will be notified by message.",
      RejectionNamePlaceholder: "Enter rejection reason",
      Accept: "Reject",
      Close: "Close",
      TitleOnUserError: "You must specify the reason of rejection."
    },
    RejectAttachmentSubject: "Rejection of dining request document",
    RejectAttachmentBody: "Document: '{{attachmentName}}' of your dining request was reject with reason: {{rejectionName}}",
    RevertAttachment: "Revert",
    DiningClubAttachmentNote: 'The Dining club or Student Welfare Committee Foundation, if it does not have a club, can request other evidence at its discretion for the financial and property situation of the individual concerned, to decide whether he is entitled to the free dining right',
    Yes: "Yes",
    No: "No",
    OutOf: "out of",
    PrintReport:"Print report"
  },
  "CreatedAt":"Created at",
  "ModifiedAt": "Modified at"
};
