/* tslint:disable max-line-length */


// tslint:disable: quotemark
export const el = {
NewRequestTemplates: {
    DiningRequestAction: {
      "Name": "Αίτηση Σίτισης",
      "Title": "Αιτήσεις Σίτισης",
      "Summary": "Αίτηση Σίτισης",
      "Description": "Αιτήσεις Σίτισης", 
      "History": "Ιστορικό", 
    },
    "NewDiningRequest":"Νέα" 
  },
  "Settings": {
    "EditItem": "Επεξεργασία"
  },
  
  "StudentStatuses": {
    "active": "Ενεργός",
    "candidate": "Φοιτητής από προεγγραφή",
    "declared": "Ανακηρύχθηκε",
    "erased": "Διαγράφηκε",
    "graduated": "Πτυχιούχος",
    "null": "-",
    "suspended": "Σε αναστολή σπουδών",
    "NotActive": "Μη ενεργός"
  },
  "Requests": {
    "ActionStatusTitle": "Κατάσταση αίτησης",
    "StartTime": "Ημερομηνία αίτησης",
    "StudentUniqueIdentifier": "ΑΠΜ",
    "StudentStatus": "Κατάσταση φοιτητή",
    "RequestedAfter": "Αιτήθηκε μετά από",
    "RequestedBefore": "Αιτήθηκε πριν από",
    "FatherName": "Όνομα πατρός",
    "MotherName": "Όνομα μητρός",
    "RequestNumber": "Αριθμός αίτησης",
    "EffectiveStatus": "Κατάσταση ελέγχου",
    "EffectiveStatusRating": "Χαρακτηρισμός κατάστασης ελέγχου",
    "RequestCode": "Κωδικός αίτησης",
    "Edit": {
      "claim": "Διεκπεραίωση αίτησης",
      "release": "Αναίρεση διεκπεραίωσης",
      "NotClaimed": "Η αίτηση αυτή δεν έχει παραληφθεί από κάποιον χρήστη. Πατήστε το πλήκτρο [Διεκπεραίωση αίτησης] για να αναλάβετε την ολοκλήρωση της αίτησης.",
      "CompletedWithErrors": {
        "Title": "Η διαδικασία ολοκληρώθηκε με σφάλματα.",
        "Description": {
          "One": "Η αποστολή του μηνύματος απέτυχε λόγω σφάλματος",
          "Many": "{{errors}} αποστολές απέτυχαν λόγω σφάλματος"
        }
      }
    },
    "Claimed": "Διεκπεραιώνεται"
  },
  "ActionStatusTypes": {
    "ActiveActionStatus": "Σε εκκρεμότητα",
    "CancelledActionStatus": "Απορρίφθηκε",
    "CompletedActionStatus": "Εγκρίθηκε",
    "FailedActionStatus": "Απέτυχε",
    "PotentialActionStatus": "Προσωρινά αποθηκευμένη",
    "null": "-"
  },
  "EffectiveStatusTypes": {
    "RejectedAttachmentsEffectiveStatus": "Μη έγκυρα έγγραφα",
    "AcceptedAttachmentsEffectiveStatus": "Έγκυρα έγγραφα",
    "UnknownStatus": " ",
    "InvalidRequestDataEffectiveStatus": "Μη έγκυρη αίτηση"
  },
  "DiningCardStatuses": {
    "Active": "Ενεργό",
    "Cancelled": "Άκυρο",
  },
  "Reports": {
    "Viewer": {
      "Close": "Κλείσιμο",
      "Print": "Εκτύπωση αρχείου",
      "Download": "Λήψη αρχείου",
      "Sign": "Ψηφιακή υπογραφή"
    },
  },
  "Students": {
    "GivenName": "Όνομα",
    "FamilyName": "Επώνυμο",
    "StudentIdentifier":"ΑΜ",
    "StudentUniqueIdentifier": "ΑΠΜ",
    "FullName": "Ονοματεπώνυμο",
    "StatusTypes": {
      "active": "Ενεργός",
      "erased": "Διαγράφηκε",
      "graduated": "Αποφοίτησε",
      "suspended": "Σε αναστολή",
      "declared": "Ανακηρύχθηκε",
      "candidate": "Από προεγγραφή"
    },
    "DepartmentAbbreviation": "Τμήμα",
    "StudentDepartment": "Τμήμα",
    "StudentCategory": "Κατηγορία φοιτητή",
    "Department": "Τμήμα",
    "studyProgram": "Πρόγραμμα Σπουδών",
    "Semester": "Εξάμηνο",
    "Specialty": "Κατεύθυνση",
    "inscriptionYear": "Έτος εγγραφής",
    "StudentName": "Ονοματεπώνυμο φοιτητή",
    "studentInstituteIdentifier": "ΑΓΜ",
    "Status": "Κατάσταση",
    "inscriptionPeriod": "Περίοδος εγγραφής",
    "General": "Γενικά",
    "GeneralInfo": "Γενικά Στοιχεία",
    "FatherName": "Όνομα Πατρός",
    "MotherName": "Όνομα Μητρός",
    "Nationality": "Υπηκοότητα",
    "Contact": "ΕΠΙΚΟΙΝΩΝΙΑ",
    "Email": "Email",
    "HomeAddressDetails": "Στοιχεία Μόνιμης Κατοικίας",
    "TemporaryAddressDetails": "Στοιχεία Προσωρινής Κατοικίας",
    "Address": "Διεύθυνση",
    "Phone": "Τηλέφωνο",
    "MobilePhone": "Κινητό τηλέφωνο",
    "City": "Πόλη",
    "PostalCode": "TK",
    "Region": "Περιοχή",
    "Country": "Χώρα",
    "Details": "Στοιχεία Καρτέλας",
    "UserName": "Όνομα χρήστη",
    "More" : "Περισσότερα",
    "SeeMore" : "Δες Περισσότερα",
    "Empty" : "Δεν υπάρχουν εγγραφές",
    "AcademicPeriod": "Ακαδημαϊκή περίοδος",
    "InscriptionMode": "Τρόπος εγγραφής",
    "CitizenInformation": "Στοιχεία Ταυτότητας",
    "CitizenRegistrar": "Αριθμός δημοτολογίου",
    "CitizenRegistrarPlace": "Τόπος δημοτολογίου",
    "CitizenRegistrarRegion": "Νομός τόπου δημοτολογίου",
    "MaleRegistrar": "Αριθμός μητρώου αρρένων",
    "MaleRegistrarPlace": "Τόπος μητρώου αρρένων",
    "MaleRegistrarRegion": "Νομός μητρώου αρρένων",
    "SpouseName": "Όνομα Συζύγου",
    "SchoolInformation": "Πληροφορίες Σχολείου (2ο Βάθμιας Εκπαίδευσης)",
    "SchoolGraduated": "Τίτλος",
    "SchoolGraduatedYear": "Έτος",
    "SchoolGraduationNumber": "Αριθμός",
    "SchoolGraduationGrade": "Βαθμός",
    "VatNumber": "ΑΦΜ",
    "VatOffice": "ΔΟΥ",
    "SSN": "ΑΜΚΑ",
    "BirthDate": "Ημερομηνία γέννησης",
    "BirthPlace": "Τόπος Γέννησης",
    "BirthPlaceRegion": "Περιοχή Γέννησης",
    "FamilyStatus": "Οικογενειακή Κατάσταση",
    "MilitaryStatus": "Στρατιωτική Κατάσταση",
    "Gender": "Φύλο",
    "RemovalDate": "Ημερομηνία διαγραφής",
    "RemovalNumber": "Αριθμός διαγραφής",
    "RemovalRequest": "Αίτηση διαγραφής",
    "RemovalDecision": "Αριθμός απόφασης διαγραφής",
    "RemovalReason": "Λόγος διαγραφής",
    "RemovalDepartment": "Τμήμα προορισμού",
    "RemovalComments": "Σχόλια διαγραφής",
    "RemovalYear": "Έτος διαγραφής",
    "RemovalPeriod": "Περίοδος διαγραφής",
    "RemoveAction": "Διαγραφή",
    "SuspendRequest": "Αίτηση αναστολής",
    "DeletionRequest": "Αίτηση διαγραφής",
    "InscriptionInformation": "ΣΤΟΙΧΕΙΑ ΕΓΓΡΑΦΗΣ",
    "InscriptionNumber": "Κωδικός υποψηφίου",
    "InscriptionModeCategory": "Κατηγορία εγγραφής",
    "InscriptionComments": "Σχόλια Εγγραφής",
    "InscriptionSemester": "Εξάμηνο εισαγωγής",
    "InscriptionYear": "Έτος εισαγωγής",
    "InscriptionIndex": "Αριθμός σειράς εισαγωγής",
    "InscriptionDecision": "Αριθμός απόφασης εισαγωγής",
    "InscriptionPoints": "Μόρια εισαγωγής",
    "InscriptionDepartment": "Τμήμα προέλευσης",
    "IdentityCard": "Ταυτότητα",
    "IdentityType": "Τύπος Εγγράφου",
    "IdentityDate": "Ημερομηνία",
    "IdentityAuthority": "Αρχή Ταυτότητας",
    "InsuranceProvider": "Φορέας Ασφάλισης",
    "InsuranceNumber": "Αριθμός Μητρώου Ασφάλισης",
    "PersonalInformation": "ΠΡΟΣΩΠΙΚΑ ΣΤΟΙΧΕΙΑ",
    "PersonalDetails": "Προσωπικές Πληροφορίες",
    "ContactDetails":"Στοιχεία Επικοινωνίας",
    "phone": "Τηλέφωνο",
    "email": "email", 
  "StudyLevel": "Επίπεδο σπουδών",
  "StudyLevels": {
    "undergraduate": "Προπτυχιακό",
    "postgraduate": "Μεταπτυχιακό",
    "specialprogram": "Άλλο",
    "doctoral": "Διδακτορικό",
    "postdoctoral": "Μεταδιδακτορικό"
    },
  },
  "Register": {
    "Accept": "Έγκριση",
    "Attachments": "Επισυνάψεις",
    "DateCreated": "Δημιουργήθηκε",
    "DateModified": "Τροποποιήθηκε",
    "DateSubmitted": "Υποβλήθηκε",
    "Details": "Λεπτομέρειες",
    "RequestDetails": "Λεπτομέρειες αίτησης σίτισης",
    "Download": "Λήψη",
    "Message": "Μήνυμα",
    "Messages": "Μηνύματα",
    "NewMessage": "Σύνταξη νέου μηνύματος",
    "NoAttachments": "Δεν υπάρχουν επισυνάψεις",
    "NoMessages": "Δεν υπάρχουν μηνύματα",

    "Preview": "Προεπισκόπηση",
    "Reject": "Απόρριψη",
    "Reset": "Επαναφορά σε προσωρινά αποθηκευμένη",
    "Revert": "Επαναφορά σε εκκρεμότητα",
    "Review": "Αξιολόγηση",
    "TotalIncomeBasedOnStudentsRequest": "Συνολικό οικογενειακό εισόδημα βάσει δήλωσης",
    "SendMessage": "Αποστολή μηνύματος",
    "Status": "Κατάσταση αίτησης",
    "RequestNumber": "Αριθμός αίτησης",
    "UploadFileHelp": "Αποθέστε το αρχείο για επισύναψη ή αναζητήστε το",
    "UploadFilesHelp": "Αποθέστε το αρχείο για επισύναψη ή αναζητήστε το",
    "ComposeNewMessage": {
      "Description": "Η διαδικασία θα προσπαθήσει να στείλει ένα μήνυμα στον υποψήφιο σχετικά με την αίτηση. Γράψτε ένα σύντομο μήνυμα και ξεκινήστε την αποστολή.",
      "Send": "Αποστολή",
      "Cancel": "Άκυρο",
      "Subject": "Θέμα",
      "Title": "Σύνταξη νέου μηνύματος",
      "WriteMessage": "Γράψτε ένα σύντομο μήνυμα",
    },
    "UserReviewAdded": "Η αίτηση έχει αξιολογηθεί",
    "addReview": "Υποβολή",
    "Edit": "Επεξεργασία",
    "NoReview": "Χωρίς αξιολόγηση",
    "Rating": "Βαθμός Αξιολόγησης",
    "Add": "Υποβολή",
    "CalculatedTotalFamilyIncome": "Υπολογιζόμενο συνολικό οικογενειακό εισόδημα" ,
    "CheckingLimit": "Όριο Ελέγχου",
    "NumberOfSiblingStudents": "Αριθμός φοιτητών αδελφών",
    "CodeTaxValue": "Κωδικός 003 από το εκκαθαριστικό",
    "TotalFamilyIncome": "Συνολικό οικογενειακό εισόδημα"
  },
  "Forms": {
    "DiningRequest": {
      "Disabled": "Α.Μ.Ε.Α",
      "LessThan25yrs": "Έως 25 ετών",
      "LivePermanentlyInSameLocationWithInstitution": "Μόνιμη Διαμονή Θεσσαλονίκη",
      "ForeignScholarStudent": "Αλλοδαπός υπότροφος",
      "Prerequisites": "Πληρoί όλες τις προϋποθέσεις",
      "Unemployment": "Εισπράτει επίδομα ανεργίας",
      "StudentMaritalStatusTitle": "Οικογενειακή Κατάσταση"
    },
    "DiningRequestAction": "Αίτηση Σίτισης",
    "active": "Ενεργός",
    "candidate": "Φοιτητής από προεγγραφή",
    "declared": "Ανακηρύχθηκε",
    "erased": "Διαγράφηκε",
    "graduated": "Πτυχιούχος",
    "suspended": "Σε αναστολή σπουδών",
    "transfered": "Ο φοιτητής έχει μεταγραφεί σε άλλο ίδρυμα",
    "single": "Άγαμος/η",
    "married": "Έγγαμος/η",
    "RejectedAttachmentsEffectiveStatus": "Μη έγκυρα έγγραφα",
    "AcceptedAttachmentsEffectiveStatus": "Έγκυρα έγγραφα",
    "UnknownStatus": "Άγνωστο",
    "InvalidRequestDataEffectiveStatus": "Μη έγκυρη αίτηση",
    "DiningUsers": "ΠΦΛ",
  },
  "Yes": "Ναι",
  "No": "Όχι",
  "summer": "Εαρινό",
  "winter": "Χειμερινό",
  "AcademicPeriod": {
    "summer": "Εαρινό",
    "winter": "Χειμερινό"
  },
  "AcademicYear": "Ακαδημαϊκό έτος", 
  "Attachments":{
    "Accept":"Έγκυρα έγγραφα",
    "Reject": "Μη έγκυρα έγγραφα",
    "UnknownStatus": "Άγνωστο",
    "InvalidRequestData": "Μη έγκυρη αίτηση"
  },
  UniversisDiningModule: {
    DiningCardsTemplates: {
      DiningCard: {
        Name: 'Δικαίωμα Σίτισης',
        Title: 'Δικαίωμα Σίτισης',
        Summary: 'Δικαίωμα Σίτισης',
        Description: 'Δικαιώματα Σίτισης',
      },
      validFrom: 'Ισχύει από',
      validThrough: 'Ισχύει έως',
      SerialNumber: 'Αρ. Δικαιώματος',
      active: 'Κατάσταση δικαιώματος σίτισης',
      AcademicPeriod: 'Aκαδημαϊκή περίοδος',
      AcademicYear: "Ακαδημαϊκό έτος", 
      CancelItem: "Ακύρωση",
      CancelItemMessage: {
        title: "Ακύρωση δικαιωμάτων",
        one: "Ένα δικαίωμα ακυρώθηκε με επιτυχία.",
        many: "{{value}} δικαιώματα ακυρώθηκαν με επιτυχία."
      },
      ReactivateItem: "Επανενεργοποίηση",
      ReactivateMessage: {
        title: "Επανενεργοποίηση δικαιωμάτων",
        one: "Ένα νέο δικαίωμα επανενεργοποιήθηκε με επιτυχία.",
        many: "{{value}} νέα δικαιώματα επανενεργοποιήθηκαν με επιτυχία."
      },
      CancelItemsTitle: "Ακύρωση στοιχείου",
      CancelItemsMessage: "Πρόκειται να ακυρώσετε ένα ή περισσότερα δικαιώματα. Θέλετε να προχωρήσετε?",
    },
    StudentDiningCardExists: "Υπάρχει εγκεκριμένη αίτηση σίτισης", 
    DiningCardStatuses: {
      true: "Ενεργό",
      false: "Άκυρο",
    },
    summer: "Εαρινό",
    PersonalInformation: 'Προσωπικά Στοιχεία',
    DiningDocuments: 'Δικαιολογητικά',
    DiningRequestTitle: 'Αίτηση Σίτισης',
    DiningRequestCapitalTitle: 'ΑΙΤΗΣΗ ΣΙΤΙΣΗΣ',
    DocumentsSubmission: {
      Title: 'Κατάθεση εγγράφων',
      Subtitle: 'Ακολούθησε τις παρακάτω οδηγίες για να καταθέσεις τα απαιτούμενα έγγραφα για την αίτηση σίτισής σου.',
      SubmissionStatus: 'Κατάσταση κατάθεσης',
      SubmissionStatuses: {
        pending: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        completed: 'Η κατάθεση εγγράφων για την σίτιση έχει ολοκληρωθεί',
        failed: 'Η κατάθεση εγγράφων για την σίτιση είναι σε εξέλιξη',
        unavailable: 'Η κατάθεση εγγράφων δεν είναι διαθέσιμη'
      },
      AttachmentDeleteModal: {
        Title: 'Διαγραφή εγγράφου',
        Body: 'Διαγραφή εγγράφου τύπου {{attachmentType}};',
        Notice: 'Αυτή η πράξη δεν είναι αναιρέσιμη.',
        Close: 'Κλείσιμο',
        Delete: 'Διαγραφή'
      },
      DiningDocumentToUpload: 'έγγραφο για μεταφόρτωση',
      DiningDocumentsToUpload: 'έγγραφα για μεταφόρτωση',
      DiningDocumentsPhysicals: 'Έγγραφα σίτισης που πρέπει να παραδοθούν στην γραμματεία του τμήματος',
      DownloadDocument: 'Λήψη εγγράφου',
      UploadDocument: 'Μεταφόρτωση',
      RemoveDocument: 'Αφαίρεση αρχείου',
      ContactService: 'Επικοινωνία με υπεύθυνο',
      Errors: {
        Download: 'Υπήρξε σφάλμα κατά την λήψη του αρχείου',
        Remove: 'Υπήρξε σφάλμα κατά την αφαίρεση του αρχείου',
        Upload: 'Υπήρξε σφάλμα κατά την μεταφόρτωση του αρχείου'
      }
    },
    ModalConfirm: {
      Submit: 'Ολοκλήρωση',
      Close: 'Κλείσιμο',
      Title: 'Αποστολή Αίτησης Σίτισης',
      Body: 'Θέλετε να στείλετε την αίτηση σας για αποστολή και έλεγχο στην γραμματεία;'
    },
    Messages: {
      Title: 'Μηνύματα',
      NewMessage: 'Νέο μήνυμα',
      Subject: 'Θέμα',
      WriteMessage: 'Το μήνυμα σας',
      NoSubject: 'Χωρίς θέμα',
      IncomingMessage: 'Εισερχόμενο μήνυμα',
      NoMessages: 'Κανένα μήνυμα',
      Info:"Από τη σελίδα αυτή μπορείτε να επικοινωνήσετε με την ΠΦΛ. Για να αποστείλετε δικαιολογητικά, πατήστε \"Νέο μήνυμα\" και επισυνάψτε τα απαραίτητα έγγραφα",
      SentAfter: "Εστάλη μετά",
      SentBefore: "Εστάλη πριν",
      Sender: "Αποστολέας",
      Recipient: "Παραλήπτης",
      SentByStudent: "Απεστάλη από φοιτητή/τρια",
      MarkAsRead: "Σήμανση ως αναγνωσμένο"
    },
    MessagePrompt: 'Το μήνυμά σου',
    Send: 'Αποστολή',
    Cancel: 'Ακύρωση',
    Date: 'Ημερομηνία',
    Time: 'Ώρα',
    Location: 'Τοποθεσία',
    Download: 'Λήψη',
    Previous: 'Προηγούμενο',
    Next: 'Επόμενο',
    Submit: 'Υποβολή',
    Completed: 'Ολοκλήρωση Αίτησης',
    ContactRegistrar: 'Επικοινωνία με Γραμματεία',
    StudentInfo: 'Στοιχεία Φοιτητή',
    StudyGuide: 'ΟΔΗΓΟΣ ΣΠΟΥΔΩΝ',
    Specialty: 'ΚΑΤΕΥΘΥΝΣΗ',
    Prerequisites: 'Προϋποθέσεις',
    Progress: 'Πρόοδος',
    NoRulesFound: 'Οι προϋποθέσεις πτυχίου δεν έχουν οριστεί.',
    CourseType: 'Τύπος Μαθημάτων',
    AllTypeCourses: 'Όλοι οι τύποι μαθημάτων',
    Thesis: 'Εργασία',
    Student: 'Ιδιότητες Φοιτητή',
    Internship: 'Πρακτική',
    Course: 'Προαπαιτούμενο Μάθημα',
    CourseArea: 'Γνωστικό Αντικείμενο',
    CourseCategory: 'Κατηγορία Μαθήματος',
    CourseSector: 'Τομέας Μαθημάτων',
    ProgramGroup: 'Ομάδα Μαθημάτων',
    StatusLabel: 'Κατάσταση της αίτησης σου',
    NoAttachments: 'Δεν υπαρχουν έγγραφα για μεταφόρτωση',
    TemporarySaveMessage: 'Η αίτησή σας αποθηκεύτηκε προσωρινά',
    AttachDocumentMessage: 'Επισυνάψτε τα δικαιολογητικά που απαιτούνται',
    RequestPeriodExpired: 'H περίοδος αιτήσεων σίτισης έχει λήξει.',
    RequestPeriodNotStarted: 'Η περίοδος αιτήσεων σίτισης θα είναι ανοιχτή από {{dateStart}} εώς {{dateEnd}}.',
    NoDiningRequestEvent: 'Η περίοδος αιτήσεων σίτισης δεν έχει οριστεί.',
    EmptyDocumentList: "Η λίστα των απαραίτητων εγγράφων είναι κενή.",
    EmptyDocumentListContinue: "Η λίστα των απαραίτητων εγγράφων είναι κενή. Μπορείτε να συνεχίσετε με την υποβολή της αίτησης.",
    VatNumberCrosscheckFailed: "Προσοχή. Ο έλεγχος 'ΑΦΜ' δεν επαληθεύτηκε",
    OutOf:"από",
    ModifiedBy: "Τροποιήθηκε από",
    AcceptedAt: "Ημερομηνία έγκρισης",
    InformationIsNotAvailable:"Η πληροφορία δεν είναι διαθέσιμη", 
    "AcceptConfirm": {
      "Title": "Έγκριση και ολοκλήρωση",
      "Message": "Πρόκειται να εγκρίνετε την αίτηση. Μετά από αυτό ο υποψήφιος θα αποκτήσει μία νέα κάρτα σίτισης. Θέλετε να συνεχίσετε;"
    },
    "RejectConfirm": {
      "Title": "Απόρριψη αίτησης",
      "Message": "Πρόκειται να απορρίψετε την αίτηση. Ο υποψήφιος θα ενημερωθεί σχετικά με την απόρριψη. Θέλετε να συνεχίσετε;"
    },
    "AcceptAction": {
      "Title": "Έγκριση αιτήσεων",
      "Description":"Η διαδικασία έγκρισης αιτήσεων θα προσπαθήσει να ολοκληρώσει τις επιλεγμένες αιτήσεις. Κατά την ολοκλήρωση των αιτήσεων θα εκτελεστούν και όλες οι προκαθορισμένες διαδικασίες για κάθε τύπο αίτησης πχ. η έκδοση και η αρχειοθέτηση εγγράφων σχετιζόμενων με την αίτηση. Η διαδικασία μπορεί να ολοκληρωθεί για αιτήσεις σε εκκρεμότητα."
    },
    "RejectAction": {
      "Title": "Απόρριψη αιτήσεων",
      "Description":"Η διαδικασία απόρριψης αιτήσεων θα προσπαθήσει να απορρίψει τις επιλεγμένες αιτήσεις. Μετά το τέλος της διαδικασίας οι χρήστες θα πληροφορηθούν για την απόρριψη της αίτησης τους από τις εφαρμογές του συστήματος. Η διαδικασία μπορεί να ολοκληρωθεί για αιτήσεις σε εκκρεμότητα."
    },
    "AcceptConfirmAttachments": {
      "Title": "Αποδοχή επισυναπτόμενων εγγράφων",
      "Message": "Πρόκειται να αποδεχτείτε τα έγγραφα. Θέλετε να συνεχίσετε;"
    },
    "RejectConfirmAttachments": {
      "Title": "Απόρριψη επισυναπτόμενων εγγράφων",
      "Message": "Πρόκειται να απορρίψετε τα έγγραφα. Θέλετε να συνεχίσετε;"
    },
    "InvalidRequestDataConfirm": {
      "Title": "Μη έγκυρη αίτηση",
      "Message": "Πρόκειται να θέσετε τα επισυναπτόμενα έγγραφα της αίτησης σε κατάσταση 'Μη έγκυρη αίτηση'. Θέλετε να συνεχίσετε;"
    },
    "UnknownEffectiveStatusConfirm": {
      "Title": "Άγνωστο",
      "Message": "Πρόκειται να θέσετε τα επισυναπτόμενα έγγραφα της αίτησης σε κατάσταση 'Άγνωστο'. Θέλετε να συνεχίσετε;"
    },
    "ResetConfirm": {
      "Title": "Αλλαγή κατάστασης αίτησης",
      "Message": "Πρόκειται να θέσετε την κατάσταση της αίτησης 'Σε εκκρεμότητα'. Μετά από αυτό ο υποψήφιος θα μπορεί να κάνει αλλαγές στην αίτηση και να την αποστείλει εκ νέου. Θέλετε να συνεχίσετε;"
    },
    "RevertConfirm": {
      "Title": "Επανεργοποίηση αίτησης",
      "Message": "Πρόκειται να ενεργοποιήσετε ξανά την αίτηση. Θέλετε να συνεχίσετε;"
    },
    "SuspendAction": {
      "Title": "Ακύρωση δικαιωμάτων σίτισης",
      "Description":"Η διαδικασία ακύρωσης δικαιωμάτων σίτισης θα προσπαθήσει να ακυρώσει τα επιλεγμένα δικαιώματα σίτισης. Κατά την ακύρωση των δικαιωμάτων σίτισης θα εκτελεστούν και όλες οι προκαθορισμένες διαδικασίες για κάθε τύπο δικαιώματος πχ. η έκδοση και η αρχειοθέτηση εγγράφων σχετιζόμενων με το δικαίωμα σίτισης. Η διαδικασία μπορεί να ολοκληρωθεί για ενεργά δικαιώματα σίτισης."
    },
    "ReactivateAction": {
      "Title": "Έγκριση αιτήσεων",
      "Description":"Η διαδικασία επανενεργοποίησης δικαιωμάτων σίτισης θα προσπαθήσει να επανενεργοποίησει τα επιλεγμένα δικαιώματα σίτισης. Κατά την επανανεργοποίηση των δικαιωμάτων σίτισης θα εκτελεστούν και όλες οι προκαθορισμένες διαδικασίες για κάθε τύπο δικαιώματος πχ. η έκδοση και η αρχειοθέτηση εγγράφων σχετιζόμενων με την αίτηση. Η διαδικασία μπορεί να ολοκληρωθεί για άκυρα δικαιώματα σίτισης."
    },
    CancelCardModal:{
      "ResetConfirm": {
        "Title": "Αλλαγή κατάστασης του δικαιώματος σίτισης",
        "Message": "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Άκυρο'. Αν θέλετε να επαναφέρετε την αντίστοιχη αίτηση σε κατάσταση της αίτησης 'Σε εκκρεμότητα'. Θέλετε να συνεχίσετε;"
      },
      Title: "Ακύρωση δικαιώματος σίτισης",
      Help: "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Άκυρο'. Αν θέλετε να επαναφέρετε την αντίστοιχη αίτηση σε κατάσταση της αίτησης 'Σε εκκρεμότητα', τότε επιλέξετε 'Ακύρωση δικαιώματος και επαναφορά αίτησης'. Ο/Η δικαιούχος θα ενημερωθεί μέσω μηνύματος.",
      CancelNamePlaceholder: "Εισάγετε τον λόγο ακύρωσης",
      Accept: "Ακύρωση δικαιώματος",
      CancelCardActiveRequest: "Ακύρωση και επαναφορά",
      Close: "Κλείσιμο",
      TitleOnUserError: "Πρέπει να δηλώσετε τον λόγο ακύρωσης."
    },
    ReactivateCardModal:{
      "ResetConfirm": {
        "Title": "Αλλαγή κατάστασης του δικαιώματος σίτισης",
        "Message": "Πρόκειται να θέσετε την κατάσταση του δικαιώματος 'Ενεργό'. Θέλετε να συνεχίσετε;"
      },
      Title: "Επανενεργοποίηση δικαιώματος σίτισης",
      Accept: "Ενεργοποίηση",
      Close: "Κλείσιμο",
    },
    CancelCardSubject: "Ακύρωση δικαιώματος σίτισης",
    CancelCardBody: "Το δικαίωμα σίτισης ακυρώθηκε λόγω: {{cancelReason}}",
    RevertCard: "Επαναφορά",
    RejectAttachmentModal: {
      Title: "Απόρριψη δικαιολογητικού",
      Help: "Δηλώστε τον λόγο απόρριψης του δικαιολογητικού. Ο/Η αιτών/αιτούσα θα ενημερωθεί μέσω μηνύματος.",
      RejectionNamePlaceholder: "Εισάγετε τον λόγο απόρριψης",
      Accept: "Απόρριψη",
      Close: "Κλείσιμο",
      TitleOnUserError: "Πρέπει να δηλώσετε τον λόγο απόρριψης."
    },
    RejectAttachmentSubject: "Απόρριψη δικαιολογητικού αίτησης σίτισης",
    RejectAttachmentBody: "Το δικαιολογητικό: '{{attachmentName}}' της αίτησης σίτισης απορρίφθηκε λόγω: {{rejectionName}}",
    RevertAttachment: "Επαναφορά",
    DiningClubAttachmentNote: 'Η Λέσχη ή η Επιτροπή Φοιτητικής Μέριμνας του Ιδρύματος, εάν σε αυτό δεν λειτουργεί Λέσχη, μπορεί να ζητά και άλλα, κατά την κρίση της αποδεικτικά στοιχεία για την οικονομική και περιουσιακή κατάσταση του ενδιαφερόμενου, προκειμένου να αποφανθεί αν δικαιούται ή όχι σίτισης.',
    Yes: "Ναι",
    No: "Όχι",
    PrintReport:"Εξαγωγή αναφοράς" 
  },
  "CreatedAt":"Δημιουργήθηκε",
  "ModifiedAt": "Τροποποιήθηκε",
  "Submit": "Υποβολή"
};